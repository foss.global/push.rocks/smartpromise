/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smartpromise',
  version: '4.0.3',
  description: 'simple promises and Deferred constructs'
}
